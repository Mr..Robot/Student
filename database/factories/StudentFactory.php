<?php

use Faker\Generator as Faker;

$factory->define(App\Student::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name(),
        'second_name' => $faker->name(),
        'last_name' => $faker->name(),
        'number' => $faker->name(),
        'join_date' => $faker->date(),
        'date_birth' => $faker->date(),
        'place_birth' => $faker->name(),
    ];
});
