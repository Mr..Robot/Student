<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'description' => $faker->paragraph(),
        'library_id' => function () {
            return factory(App\Library::class)->create()->id;
        },
    ];
});
