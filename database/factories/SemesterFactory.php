<?php

use Faker\Generator as Faker;

$factory->define(App\Semester::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
    ];
});
