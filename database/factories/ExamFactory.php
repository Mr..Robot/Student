<?php

use Faker\Generator as Faker;

$factory->define(App\Exam::class, function (Faker $faker) {
    return [
        'date' => $faker->date(),
        'time' => $faker->time(),
        'subject_id' => function () {
            return factory(App\Subject::class)->create()->id;
        },
    ];
});
