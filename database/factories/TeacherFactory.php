<?php

use Faker\Generator as Faker;

$factory->define(App\Teacher::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name(),
        'second_name' => $faker->name(),
        'last_name' => $faker->name(),
    ];
});
