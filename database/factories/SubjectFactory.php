<?php

use Faker\Generator as Faker;

$factory->define(App\Subject::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'code' => substr($faker->name(), 0, 10),
        'department_id' => function () {
            return factory(App\Department::class)->create()->id;
        },
    ];
});
