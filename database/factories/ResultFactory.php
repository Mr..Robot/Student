<?php

use Faker\Generator as Faker;

$factory->define(App\Result::class, function (Faker $faker) {
    return [
        'score' => $faker->randomDigitNotNull(),
        'student_id' => function () {
            return factory(App\Student::class)->create()->id;
        },
        'exam_id' => function () {
            return factory(App\Exam::class)->create()->id;
        },
    ];
});
