<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Book::class, 5)->create()->each(function ($book) {
            $book->addMediaFromUrl('http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf')
                ->toMediaCollection('file')
            ;
        });
    }
}
