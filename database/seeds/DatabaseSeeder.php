<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(SemesterTableSeeder::class);
        $this->call(SubjectTableSeeder::class);
        $this->call(StudentTableSeeder::class);
        $this->call(ExamTableSeeder::class);
        $this->call(AdTableSeeder::class);
        $this->call(TeacherTableSeeder::class);
        $this->call(ResultTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(LibraryTableSeeder::class);
        $this->call(BookTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
    }
}
