<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Library extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'department_id'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'name' => 'required|string',
            'department_id' => 'required|numeric|exists:departments,id',
        ];
    }

    /**
     * Get the department for the Library.
     */
    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    /**
     * Get the books for the Library.
     */
    public function books()
    {
        return $this->hasMany('App\Book');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['department'])->paginate(10);
    }
}
