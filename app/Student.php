<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'second_name', 'last_name', 'number', 'join_date', 'date_birth', 'place_birth'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'first_name' => 'required|string',
            'second_name' => 'required|string',
            'last_name' => 'required|string',
            'number' => 'required|string',
            'join_date' => 'required|date',
            'date_birth' => 'required|date',
            'place_birth' => 'required|string',
        ];
    }

    /**
     * Get the results for the Student.
     */
    public function results()
    {
        return $this->hasMany('App\Result');
    }

    /**
     * Get the exams for the Student.
     */
    public function exams()
    {
        return $this->belongsToMany('App\Exam');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::paginate(10);
    }
}
