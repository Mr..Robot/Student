<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'department_id'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'name' => 'required|string',
            'code' => 'required|string|max:10',
            'department_id' => 'required|numeric|exists:departments,id',
        ];
    }

    /**
     * Get the exams for the Subject.
     */
    public function exams()
    {
        return $this->hasMany('App\Exam');
    }

    /**
     * Get the department for the Subject.
     */
    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    /**
     * Get the semesters for the Subject.
     */
    public function semesters()
    {
        return $this->belongsToMany('App\Semester');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['department'])->paginate(10);
    }
}
