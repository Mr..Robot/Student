<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'time', 'subject_id'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'date' => 'required|date',
            'time' => 'required|date_format:H:i:s',
            'subject_id' => 'required|numeric|exists:subjects,id',
            'students' => 'required|array',
            'students.*' => 'required|numeric|exists:students,id',
        ];
    }

    /**
     * Get the subject for the Exam.
     */
    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    /**
     * Get the results for the Exam.
     */
    public function results()
    {
        return $this->hasMany('App\Result');
    }

    /**
     * Get the students for the Exam.
     */
    public function students()
    {
        return $this->belongsToMany('App\Student');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['subject'])->paginate(10);
    }
}
