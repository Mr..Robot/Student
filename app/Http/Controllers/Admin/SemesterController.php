<?php

namespace App\Http\Controllers\Admin;

use App\Semester;
use App\Subject;
use App\Http\Controllers\Controller;

class SemesterController extends Controller
{
    /**
     * Display a list of Semesters.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $semesters = Semester::getList();

        return view('admin.semesters.index', compact('semesters'));
    }

    /**
     * Show the form for creating a new Semester
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = Subject::all();

        return view('admin.semesters.add', compact('subjects'));
    }

    /**
     * Save new Semester
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Semester::validationRules());

        unset($validatedData['subjects']);
        $semester = Semester::create($validatedData);

        $semester->subjects()->sync(request('subjects'));

        return redirect()->route('admin.semesters.index')->with([
            'type' => 'success',
            'message' => 'Semester added'
        ]);
    }

    /**
     * Show the form for editing the specified Semester
     *
     * @param \App\Semester $semester
     * @return \Illuminate\Http\Response
     */
    public function edit(Semester $semester)
    {
        $subjects = Subject::all();

        $semester->subjects = $semester->subjects->pluck('id')->toArray();

        return view('admin.semesters.edit', compact('semester', 'subjects'));
    }

    /**
     * Update the Semester
     *
     * @param \App\Semester $semester
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Semester $semester)
    {
        $validatedData = request()->validate(
            Semester::validationRules($semester->id)
        );

        unset($validatedData['subjects']);
        $semester->update($validatedData);

        $semester->subjects()->sync(request('subjects'));

        return redirect()->route('admin.semesters.index')->with([
            'type' => 'success',
            'message' => 'Semester Updated'
        ]);
    }

    /**
     * Delete the Semester
     *
     * @param \App\Semester $semester
     * @return void
     */
    public function destroy(Semester $semester)
    {
        if ($semester->subjects()->count()) {
            return redirect()->route('admin.semesters.index')->with([
                'type' => 'error',
                'message' => 'This record cannot be deleted as there are relationship dependencies.'
            ]);
        }

        $semester->delete();

        return redirect()->route('admin.semesters.index')->with([
            'type' => 'success',
            'message' => 'Semester deleted successfully'
        ]);
    }
}
