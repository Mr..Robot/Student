<?php

namespace App\Http\Controllers\Admin;

use App\Book;
use App\Library;
use App\Http\Controllers\Controller;

class BookController extends Controller
{
    /**
     * Display a list of Books.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::getList();

        return view('admin.books.index', compact('books'));
    }

    /**
     * Show the form for creating a new Book
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $libraries = Library::all();

        return view('admin.books.add', compact('libraries'));
    }

    /**
     * Save new Book
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Book::validationRules());

        unset($validatedData['file']);
        $book = Book::create($validatedData);

        $book->addMediaFromRequest('file')->toMediaCollection('file');

        return redirect()->route('admin.books.index')->with([
            'type' => 'success',
            'message' => 'Book added'
        ]);
    }

    /**
     * Show the form for editing the specified Book
     *
     * @param \App\Book $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        $libraries = Library::all();

        return view('admin.books.edit', compact('book', 'libraries'));
    }

    /**
     * Update the Book
     *
     * @param \App\Book $book
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Book $book)
    {
        $validatedData = request()->validate(
            Book::validationRules($book->id)
        );

        unset($validatedData['file']);
        $book->update($validatedData);

        $book->addMediaFromRequest('file')->toMediaCollection('file');

        return redirect()->route('admin.books.index')->with([
            'type' => 'success',
            'message' => 'Book Updated'
        ]);
    }

    /**
     * Delete the Book
     *
     * @param \App\Book $book
     * @return void
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return redirect()->route('admin.books.index')->with([
            'type' => 'success',
            'message' => 'Book deleted successfully'
        ]);
    }
}
