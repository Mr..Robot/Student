<?php

namespace App\Http\Controllers\Admin;

use App\Exam;
use App\Subject;
use App\Student;
use App\Http\Controllers\Controller;

class ExamController extends Controller
{
    /**
     * Display a list of Exams.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exams = Exam::getList();

        return view('admin.exams.index', compact('exams'));
    }

    /**
     * Show the form for creating a new Exam
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = Subject::all();
        $students = Student::all();

        return view('admin.exams.add', compact('subjects', 'students'));
    }

    /**
     * Save new Exam
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Exam::validationRules());

        unset($validatedData['students']);
        $exam = Exam::create($validatedData);

        $exam->students()->sync(request('students'));

        return redirect()->route('admin.exams.index')->with([
            'type' => 'success',
            'message' => 'Exam added'
        ]);
    }

    /**
     * Show the form for editing the specified Exam
     *
     * @param \App\Exam $exam
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam $exam)
    {
        $subjects = Subject::all();
        $students = Student::all();

        $exam->students = $exam->students->pluck('id')->toArray();

        return view('admin.exams.edit', compact('exam', 'subjects', 'students'));
    }

    /**
     * Update the Exam
     *
     * @param \App\Exam $exam
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Exam $exam)
    {
        $validatedData = request()->validate(
            Exam::validationRules($exam->id)
        );

        unset($validatedData['students']);
        $exam->update($validatedData);

        $exam->students()->sync(request('students'));

        return redirect()->route('admin.exams.index')->with([
            'type' => 'success',
            'message' => 'Exam Updated'
        ]);
    }

    /**
     * Delete the Exam
     *
     * @param \App\Exam $exam
     * @return void
     */
    public function destroy(Exam $exam)
    {
        if ($exam->students()->count() || $exam->results()->count()) {
            return redirect()->route('admin.exams.index')->with([
                'type' => 'error',
                'message' => 'This record cannot be deleted as there are relationship dependencies.'
            ]);
        }

        $exam->delete();

        return redirect()->route('admin.exams.index')->with([
            'type' => 'success',
            'message' => 'Exam deleted successfully'
        ]);
    }
}
