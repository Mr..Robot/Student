<?php

namespace App\Http\Controllers\Admin;

use App\Result;
use App\Student;
use App\Exam;
use App\Http\Controllers\Controller;

class ResultController extends Controller
{
    /**
     * Display a list of Results.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Result::getList();

        return view('admin.results.index', compact('results'));
    }

    /**
     * Show the form for creating a new Result
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Student::all();
        $exams = Exam::all();

        return view('admin.results.add', compact('students', 'exams'));
    }

    /**
     * Save new Result
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Result::validationRules());

        $result = Result::create($validatedData);

        return redirect()->route('admin.results.index')->with([
            'type' => 'success',
            'message' => 'Result added'
        ]);
    }

    /**
     * Show the form for editing the specified Result
     *
     * @param \App\Result $result
     * @return \Illuminate\Http\Response
     */
    public function edit(Result $result)
    {
        $students = Student::all();
        $exams = Exam::all();

        return view('admin.results.edit', compact('result', 'students', 'exams'));
    }

    /**
     * Update the Result
     *
     * @param \App\Result $result
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Result $result)
    {
        $validatedData = request()->validate(
            Result::validationRules($result->id)
        );

        $result->update($validatedData);

        return redirect()->route('admin.results.index')->with([
            'type' => 'success',
            'message' => 'Result Updated'
        ]);
    }

    /**
     * Delete the Result
     *
     * @param \App\Result $result
     * @return void
     */
    public function destroy(Result $result)
    {
        $result->delete();

        return redirect()->route('admin.results.index')->with([
            'type' => 'success',
            'message' => 'Result deleted successfully'
        ]);
    }
}
