<?php

namespace App\Http\Controllers\Admin;

use App\Library;
use App\Department;
use App\Http\Controllers\Controller;

class LibraryController extends Controller
{
    /**
     * Display a list of Libraries.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $libraries = Library::getList();

        return view('admin.libraries.index', compact('libraries'));
    }

    /**
     * Show the form for creating a new Library
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();

        return view('admin.libraries.add', compact('departments'));
    }

    /**
     * Save new Library
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Library::validationRules());

        $library = Library::create($validatedData);

        return redirect()->route('admin.libraries.index')->with([
            'type' => 'success',
            'message' => 'Library added'
        ]);
    }

    /**
     * Show the form for editing the specified Library
     *
     * @param \App\Library $library
     * @return \Illuminate\Http\Response
     */
    public function edit(Library $library)
    {
        $departments = Department::all();

        return view('admin.libraries.edit', compact('library', 'departments'));
    }

    /**
     * Update the Library
     *
     * @param \App\Library $library
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Library $library)
    {
        $validatedData = request()->validate(
            Library::validationRules($library->id)
        );

        $library->update($validatedData);

        return redirect()->route('admin.libraries.index')->with([
            'type' => 'success',
            'message' => 'Library Updated'
        ]);
    }

    /**
     * Delete the Library
     *
     * @param \App\Library $library
     * @return void
     */
    public function destroy(Library $library)
    {
        if ($library->books()->count()) {
            return redirect()->route('admin.libraries.index')->with([
                'type' => 'error',
                'message' => 'This record cannot be deleted as there are relationship dependencies.'
            ]);
        }

        $library->delete();

        return redirect()->route('admin.libraries.index')->with([
            'type' => 'success',
            'message' => 'Library deleted successfully'
        ]);
    }
}
