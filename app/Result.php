<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Result extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'score', 'student_id', 'exam_id'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'score' => 'required|numeric',
            'student_id' => 'required|numeric|exists:students,id',
            'exam_id' => 'required|numeric|exists:exams,id',
        ];
    }

    /**
     * Get the student for the Result.
     */
    public function student()
    {
        return $this->belongsTo('App\Student');
    }

    /**
     * Get the exam for the Result.
     */
    public function exam()
    {
        return $this->belongsTo('App\Exam');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['student'])->paginate(10);
    }
}
