## student

This application is built using Laravel.

### Installation
1. Setup a site in your local environment.
2. Create a database.
3. Copy and extract the .zip file in the project directory.
4. Set config options in the `.env` file.
5. Run `bash install.sh` in the terminal. (You can also run commands of the install.sh file one-by-one manually)

### Access

Open `yoursiteurl.dev/admin` to access the admin panel.
- Username: `admin`
- Password: `123456`

### Details about the app
#### Admin panel theme: [Core Ui](https://github.com/coreui/coreui-free-bootstrap-admin-template)

#### Models
1. Semester
2. Subject
3. Student
4. Exam
5. Ad
6. Teacher
7. Result
8. Department
9. Library
10. Book

#### Packages
1. [Laravel Media Library](https://github.com/spatie/laravel-medialibrary)
