@extends('admin.layouts.app', ['page' => 'result'])

@section('title', 'Edit Result')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Edit Result
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.results.update', ['result' => $result->id]) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="score">Score</label>
            <input type="number"
                class="form-control"
                name="score"
                required
                placeholder="Score"
                value="{{ old('score', $result->score) }}"
                step="any"
                id="score"
            >
        </div>

        <div class="form-group">
            <label for="student-id">Student</label>
            <select class="form-control"
                name="student_id"
                required
                id="student-id"
            >
                @foreach ($students as $student)
                    <option value="{{ $student->id }}"
                        {{ old('student_id', $result->student_id) == $student->id ? 'selected' : '' }}
                    >
                        {{ $student->first_name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="exam-id">Exam</label>
            <select class="form-control"
                name="exam_id"
                required
                id="exam-id"
            >
                @foreach ($exams as $exam)
                    <option value="{{ $exam->id }}"
                        {{ old('exam_id', $result->exam_id) == $exam->id ? 'selected' : '' }}
                    >
                        {{ $exam->id }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Update
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.results.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
