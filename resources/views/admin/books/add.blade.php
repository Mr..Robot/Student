@extends('admin.layouts.app', ['page' => 'book'])

@section('title', 'Add New Book')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Add New Book
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('admin.books.store') }}">
        @csrf

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text"
                class="form-control"
                name="name"
                required
                placeholder="Name"
                value="{{ old('name') }}"
                id="name"
            >
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control"
                name="description"
                id="description"
                required
                placeholder="Description"
            >{{ old('description') }}</textarea>
        </div>

        <div class="form-group">
            <label for="file">File</label>
            <input type="file"
                class="form-control"
                name="file"
                required
                value="{{ old('file') }}"
                id="file"
            >
        </div>

        <div class="form-group">
            <label for="library-id">Library</label>
            <select class="form-control"
                name="library_id"
                required
                id="library-id"
            >
                @foreach ($libraries as $library)
                    <option value="{{ $library->id }}"
                        {{ old('library_id') == $library->id ? 'selected' : '' }}
                    >
                        {{ $library->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Submit
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.books.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
