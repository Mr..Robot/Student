@extends('admin.layouts.app', ['page' => 'subject'])

@section('title', 'Add New Subject')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Add New Subject
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.subjects.store') }}">
        @csrf

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text"
                class="form-control"
                name="name"
                required
                placeholder="Name"
                value="{{ old('name') }}"
                id="name"
            >
        </div>

        <div class="form-group">
            <label for="code">Code</label>
            <input type="text"
                class="form-control"
                name="code"
                required
                placeholder="Code"
                value="{{ old('code') }}"
                id="code"
            >
        </div>

        <div class="form-group">
            <label for="department-id">Department</label>
            <select class="form-control"
                name="department_id"
                required
                id="department-id"
            >
                @foreach ($departments as $department)
                    <option value="{{ $department->id }}"
                        {{ old('department_id') == $department->id ? 'selected' : '' }}
                    >
                        {{ $department->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Submit
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.subjects.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
