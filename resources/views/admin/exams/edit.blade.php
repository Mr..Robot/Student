@extends('admin.layouts.app', ['page' => 'exam'])

@section('title', 'Edit Exam')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Edit Exam
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.exams.update', ['exam' => $exam->id]) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="date">Date</label>
            <input type="date"
                class="form-control"
                name="date"
                required
                placeholder="Date"
                value="{{ old('date', $exam->date) }}"
                id="date"
            >
        </div>

        <div class="form-group">
            <label for="time">Time</label>
            <input type="time"
                class="form-control"
                name="time"
                required
                placeholder="Time"
                value="{{ old('time', $exam->time) }}"
                step="2"
                id="time"
            >
        </div>

        <div class="form-group">
            <label for="subject-id">Subject</label>
            <select class="form-control"
                name="subject_id"
                required
                id="subject-id"
            >
                @foreach ($subjects as $subject)
                    <option value="{{ $subject->id }}"
                        {{ old('subject_id', $exam->subject_id) == $subject->id ? 'selected' : '' }}
                    >
                        {{ $subject->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="students">Student</label>
            <select class="form-control"
                name="students[]"
                required
                multiple
                id="students"
            >
                @foreach ($students as $student)
                    <option value="{{ $student->id }}"
                        {{ in_array($student->id, old('students', $exam->students)) ? 'selected' : '' }}
                    >
                        {{ $student->first_name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Update
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.exams.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
