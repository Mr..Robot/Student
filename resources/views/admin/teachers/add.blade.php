@extends('admin.layouts.app', ['page' => 'teacher'])

@section('title', 'Add New Teacher')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Add New Teacher
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.teachers.store') }}">
        @csrf

        <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text"
                class="form-control"
                name="first_name"
                required
                placeholder="First Name"
                value="{{ old('first_name') }}"
                id="first_name"
            >
        </div>

        <div class="form-group">
            <label for="second_name">Second Name</label>
            <input type="text"
                class="form-control"
                name="second_name"
                required
                placeholder="Second Name"
                value="{{ old('second_name') }}"
                id="second_name"
            >
        </div>

        <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text"
                class="form-control"
                name="last_name"
                required
                placeholder="Last Name"
                value="{{ old('last_name') }}"
                id="last_name"
            >
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Submit
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.teachers.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
