@extends('admin.layouts.app', ['page' => 'library'])

@section('title', 'Edit Library')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Edit Library
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.libraries.update', ['library' => $library->id]) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text"
                class="form-control"
                name="name"
                required
                placeholder="Name"
                value="{{ old('name', $library->name) }}"
                id="name"
            >
        </div>

        <div class="form-group">
            <label for="department-id">Department</label>
            <select class="form-control"
                name="department_id"
                required
                id="department-id"
            >
                @foreach ($departments as $department)
                    <option value="{{ $department->id }}"
                        {{ old('department_id', $library->department_id) == $department->id ? 'selected' : '' }}
                    >
                        {{ $department->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Update
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.libraries.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
