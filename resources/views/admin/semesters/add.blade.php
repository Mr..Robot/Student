@extends('admin.layouts.app', ['page' => 'semester'])

@section('title', 'Add New Semester')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Add New Semester
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.semesters.store') }}">
        @csrf

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text"
                class="form-control"
                name="name"
                required
                placeholder="Name"
                value="{{ old('name') }}"
                id="name"
            >
        </div>

        <div class="form-group">
            <label for="subjects">Subject</label>
            <select class="form-control"
                name="subjects[]"
                required
                multiple
                id="subjects"
            >
                @foreach ($subjects as $subject)
                    <option value="{{ $subject->id }}"
                        {{ is_array(old('subjects')) && in_array($subject->id, old('subjects')) ? 'selected' : '' }}
                    >
                        {{ $subject->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Submit
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.semesters.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
