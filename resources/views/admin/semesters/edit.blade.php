@extends('admin.layouts.app', ['page' => 'semester'])

@section('title', 'Edit Semester')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Edit Semester
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.semesters.update', ['semester' => $semester->id]) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name">Name</label>
            <input type="text"
                class="form-control"
                name="name"
                required
                placeholder="Name"
                value="{{ old('name', $semester->name) }}"
                id="name"
            >
        </div>

        <div class="form-group">
            <label for="subjects">Subject</label>
            <select class="form-control"
                name="subjects[]"
                required
                multiple
                id="subjects"
            >
                @foreach ($subjects as $subject)
                    <option value="{{ $subject->id }}"
                        {{ in_array($subject->id, old('subjects', $semester->subjects)) ? 'selected' : '' }}
                    >
                        {{ $subject->name }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Update
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.semesters.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
