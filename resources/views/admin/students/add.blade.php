@extends('admin.layouts.app', ['page' => 'student'])

@section('title', 'Add New Student')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Add New Student
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.students.store') }}">
        @csrf

        <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text"
                class="form-control"
                name="first_name"
                required
                placeholder="First Name"
                value="{{ old('first_name') }}"
                id="first_name"
            >
        </div>

        <div class="form-group">
            <label for="second_name">Second Name</label>
            <input type="text"
                class="form-control"
                name="second_name"
                required
                placeholder="Second Name"
                value="{{ old('second_name') }}"
                id="second_name"
            >
        </div>

        <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text"
                class="form-control"
                name="last_name"
                required
                placeholder="Last Name"
                value="{{ old('last_name') }}"
                id="last_name"
            >
        </div>

        <div class="form-group">
            <label for="number">Number</label>
            <input type="text"
                class="form-control"
                name="number"
                required
                placeholder="Number"
                value="{{ old('number') }}"
                id="number"
            >
        </div>

        <div class="form-group">
            <label for="join_date">Join Date</label>
            <input type="date"
                class="form-control"
                name="join_date"
                required
                placeholder="Join Date"
                value="{{ old('join_date') }}"
                id="join_date"
            >
        </div>

        <div class="form-group">
            <label for="date_birth">Date Birth</label>
            <input type="date"
                class="form-control"
                name="date_birth"
                required
                placeholder="Date Birth"
                value="{{ old('date_birth') }}"
                id="date_birth"
            >
        </div>

        <div class="form-group">
            <label for="place_birth">Place Birth</label>
            <input type="text"
                class="form-control"
                name="place_birth"
                required
                placeholder="Place Birth"
                value="{{ old('place_birth') }}"
                id="place_birth"
            >
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Submit
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.students.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
