@extends('admin.layouts.app', ['page' => 'ad'])

@section('title', 'Edit Ad')

@section('content')
<div class="card-header">
    <div class="row">
        <div class="col-6 pt-2 h5">
            <i class="fa fa-tint"></i>
            Edit Ad
        </div>
    </div>
</div>

<div class="card-body m-2">
    <form role="form" method="POST" action="{{ route('admin.ads.update', ['ad' => $ad->id]) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="title">Title</label>
            <input type="text"
                class="form-control"
                name="title"
                required
                placeholder="Title"
                value="{{ old('title', $ad->title) }}"
                id="title"
            >
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control"
                name="description"
                id="description"
                required
                placeholder="Description"
            >{{ old('description', $ad->description) }}</textarea>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-primary">
                Update
            </button>

            <a class="btn btn-sm btn-danger"
                href="{{ route('admin.ads.index') }}"
            >
                Cancel
            </a>
        </div>
    </form>
</div>
@endsection
